function [config] = Config_File()

%CONFIG_FILE Generate configuration for participant data analysis.
%   This function sets up the configuration used for both single and multiple
%   data analysis runs. It initializes the configuration structure with default
%   paths, analysis settings, and participant group information.

%Set to true if running for single data analysis, set to false if for
%multiple participant data analysis
config.single = false;

%Single experiment file name
config.subjectID = '8';

%Data directory for where single participant raw data is stored
config.dataDirectory = 'C:\data\APE\';

%Type of participant, varies by experiment condition. Either responderScreen,rTMS1,rTMs2,rTMS3
config.singleParticipantType = 'rTMS1';

%For multiple participant program - the title of the MAT File made for R
config.title = 'responderSCrrenTestFull';

% Directory where multiple participant raw data is stored
config.dataDirectoryMult = 'C:\data\APE_DATA\';

% Directory where to save data processed for R analysis
config.saveForRDirectory = 'C:\data\ForR\';

%If it cut off on the end phase during experiment, set to true and speify
%what trial it crashed at. Note this is only because we had this happen in
%some of the experiments, it may never happen for you
config.endPhaseFail = false;
%Trial where it cut off
config.endPhaseTrial = 15;

%Whether or not you would like to specify which trials in an experiment you
%would like to analyze: on a phase-by-phase basis - If a phase is set to false
%then all the trials are used for that phase, regardless of the numbers below.
%**Make sure the numbers you pick fall within the number of trials for that
%phase, otherwise an error occurs

config.specifyStart = false;
config.specifyRamp = false;
config.specifyStay = false;
config.specifyEnd = false;

if(config.specifyStart)
    config.startBeginTrial = 10;
    config.startEndingTrial = 18;
end

if(config.specifyRamp)
    config.rampBeginTrial = 10;
    config.rampEndingTrial = 18;
end

if(config.specifyStay)
    config.stayBeginTrial = 10;
    config.stayEndingTrial = 18;
end


if(config.specifyEnd)
    config.endBeginTrial = 1;
    config.endEndingTrial = 10;
end


%Whether or not to create a text info file
config.createInfoFile = true;

%Where to save the text info file
config.infoFileLoc = 'participantInfo\';

%Whether or not to show information about the participant in the text info
%file - ex. Date of birth. This may be useful for researcher blinding
config.showParticipantInfo = true;


%What percent of the formant trace to analyze
config.percOfFormantTrace = [0.4 0.6];
config.remDiscontinuousTrials = false;

%Whether or not to smooth each formant trace and which method
config.smoothTrace = false;
config.smoothTraceType = 'loess';

%Whether or not to remove outliers within the formant trace
config.trialRMOutliers = true;
config.trialRMOutliersType = 'quartiles';

%Whether or not to remove within phase, between trials - For example comparing 30
%start phase trials
config.phaseRMOutliers = true;
config.phaseRMOutliersType = 'quartiles';

%Whether or not to remove circular
%outliers and by what multiple of the standard deviation to do so, for both
%single experiment and multiple.
config.circularOutliers = false;
config.circularTolerance = 2;
config.circularToleranceMult = 2;

%percentage of the perturbation that the participant should compensate. This
%depends specifically on the study and has no impact on data anlysis, merely used as a visual tool for analysis.
config.percCompF1 = 15;
config.percCompF2 = 15;


%For multiple subject analysis

%Smooth formant values between subjects
config.smoothSubjects = false;
config.smoothSubjectsType = 'loess';

%Remove outliers by your own method between subjects
config.subjectRMOutliers = false;
config.subjectRMOutliersType = 'quartiles';

%Customizable conditions for a multiple experiment analysis
config.phaseByPhaseGraph = false;
config.circularGraphMult = false;

%Threshold in order to be considered a responder or non-responder or
%follower
config.thresholdF1 = 15;
config.thresholdF2 = 15;


%Either responderScreen,nonResponder, or default (rTMS1,rTMS2,rTMS3)
config.responderScreen.analyze = true;
config.nonResponder.analyze = false;
config.follower.analyze = false;

%Which conditions to analyze.
config.A1.analyze = false;
config.A2.analyze = false;
config.A3.analyze = false;
config.conditionCompare = false;

% Control group participants
% A1, A2, and A3 are control groups for the respective conditions in a multiple
% participant analysis setting.
config.A1.participants = [37 37 37 37 37];
config.A2.participants = [37 37 37 37 37];
config.A3.participants = [37 37 37 37 37 37];

%config.responderScreen.participants = [1 2 5 8 9 12 13 14 16 17 18 22 23 24 25 26 27 28 29 31 33 36 37 40];
config.responderScreen.participants = [1 2 40];
config.nonResponder.participants = [1 2 3 4];
config.follower.participants = [26 29];

%No need to edit this ->
config.responderScreen.type = 'responderScreen';
config.nonResponder.type = 'nonResponder';
config.follower.type = 'follower';
config.A1.type = 'rTMS1';
config.A2.type = 'rTMS2';
config.A3.type = 'rTMS3';
%<-

end

