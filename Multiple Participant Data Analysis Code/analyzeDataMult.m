function [params, origData, data, finalData] = analyzeDataMult(type,condition,config)

%Load all the Data
for i = 1:length(condition.participants)
    name = append(type,'/',num2str(condition.participants(i),'%i'));
    disp(name)
    [params(i)] = loadRawData(config,name);
    [origData(i), data(i)] = analyzeData(params(i),config);
    
    finalData.startPhase.F1(i) = data(i).startPhase.meanF1;
    finalData.rampPhase.F1(i) = data(i).rampPhase.meanF1;
    finalData.stayPhase.F1(i) = data(i).stayPhase.meanF1;
    finalData.endPhase.F1(i) = data(i).endPhase.meanF1;
    
    finalData.startPhase.F2(i) = data(i).startPhase.meanF2;
    finalData.rampPhase.F2(i) = data(i).rampPhase.meanF2;
    finalData.stayPhase.F2(i) = data(i).stayPhase.meanF2;
    finalData.endPhase.F2(i) = data(i).endPhase.meanF2;
end

finalData.startPhase.phase = 'Start';

finalData.rampPhase.phase = 'Ramp';

finalData.stayPhase.phase = 'Stay';

finalData.endPhase.phase = 'End';

[finalData.startPhase] = mkSpecPhaseDataStructMult(finalData.startPhase,type,config,params(1));
[finalData.rampPhase] = mkSpecPhaseDataStructMult(finalData.rampPhase,type,config,params(1));
[finalData.stayPhase] = mkSpecPhaseDataStructMult(finalData.stayPhase,type,config,params(1));
[finalData.endPhase] = mkSpecPhaseDataStructMult(finalData.endPhase,type,config,params(1));

end

