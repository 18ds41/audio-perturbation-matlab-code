function [handle] = graphByPhase(finalData, color,spacing)

yValuesF1 = [finalData.rampPhase.meanF1, finalData.stayPhase.meanF1, finalData.endPhase.meanF1];
yValuesF2 = [finalData.rampPhase.meanF2, finalData.stayPhase.meanF2, finalData.endPhase.meanF2];

cSTDF1 = [finalData.rampPhase.stdF1 finalData.stayPhase.stdF1 finalData.endPhase.stdF1];
cSTDF2 = [finalData.rampPhase.stdF2 finalData.stayPhase.stdF2 finalData.endPhase.stdF2];

%title(append('F1 and F2 frequencies for phase: ',num2str()));

names = {'Ramp'; 'Stay'; 'End'};

if(strcmp(color,'blue') == 1)
    errorbar(spacing,yValuesF1,cSTDF1,'blue','LineStyle', '-.');
    errorbar(spacing,yValuesF2,cSTDF2,'blue','LineStyle', '-.');
elseif(strcmp(color,'green') == 1)
    errorbar(spacing,yValuesF1,cSTDF1,'green','LineStyle', '-.');
    errorbar(spacing,yValuesF2,cSTDF2,'green','LineStyle', '-.');
elseif(strcmp(color,'purple') == 1)
    errorbar(spacing,yValuesF1,cSTDF1,'magenta','LineStyle', '-.');
    errorbar(spacing,yValuesF2,cSTDF2,'magenta','LineStyle', '-.');
else
    errorbar(spacing,yValuesF1,cSTDF1,'black','LineStyle', '-.');
    errorbar(spacing,yValuesF2,cSTDF2,'black','LineStyle', '-.');
end

hold on;

if(strcmp(color,'blue') == 1)
    handle = plot(spacing, yValuesF2,'Color','blue','LineStyle', '-.','Marker', '.', 'MarkerSize', 15);
    plot(spacing, yValuesF1,'Color','blue','LineStyle', '-.','Marker', '.', 'MarkerSize', 15);
elseif(strcmp(color,'green'))
    handle = plot(spacing, yValuesF2,'Color','green','LineStyle', '-.', 'Marker', '.','MarkerSize', 15);
    plot(spacing, yValuesF1,'Color','green','LineStyle', '-.', 'Marker', '.','MarkerSize', 15);
elseif(strcmp(color,'magenta'))
    handle = plot(spacing, yValuesF2,'Color','magenta','LineStyle', '-.', 'Marker', '.','MarkerSize', 15);
    plot(spacing, yValuesF1,'Color','magenta','LineStyle', '-.', 'Marker', '.','MarkerSize', 15);
else
    handle = plot(spacing, yValuesF2,'Color','black','LineStyle', '-.','Marker', '.', 'MarkerSize', 15);
    plot(spacing, yValuesF1,'Color','black','LineStyle', '-.','Marker', '.', 'MarkerSize', 15);
end


set(gca,'xtick',spacing,'xticklabel',names)

set(gca, 'ytick', 200:100:2200);

xlabel('Phase');
ylabel('Formant Frequencies');

plot(1:5,700,'b.','MarkerSize',0.00000001);

grid on;

end

