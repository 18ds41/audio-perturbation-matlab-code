function graphByPhasePerGroup(finalData,config)
       
    hold on;
    f = figure;
    spacing  = 2:4;
    handleA1 = 0;
    handleA3 = 0;
    handleA2 = 0;
    tempForMeanF1 = [];
    tempForMeanF2 = [];

    if(config.A1.analyze)
        color = 'blue';
        handleA1 = graphByPhase( finalData.A1, color,spacing);
        legend(handleA1,'A1');
        tempForMeanF1 = [tempForMeanF1 finalData.A1.startPhase.meanF1];
        tempForMeanF2 = [tempForMeanF2 finalData.A1.startPhase.meanF2];
    end
    if(config.A3.analyze)
        color = 'green';
        handleA3 = graphByPhase(finalData.A3, color,spacing);
        legend(handleA3,'A3');
        tempForMeanF1 = [tempForMeanF1 finalData.A3.startPhase.meanF1];
        tempForMeanF2 = [tempForMeanF2 finalData.A3.startPhase.meanF2];
    end
    if(config.A2.analyze)
        color = 'magenta';
        handleA2 = graphByPhase(finalData.A2, color,spacing);
        tempForMeanF1 = [tempForMeanF1 finalData.A2.startPhase.meanF1];
        tempForMeanF2 = [tempForMeanF2 finalData.A2.startPhase.meanF2];
    end
    
    stdTempF1 = std(tempForMeanF1);
    stdTempF2 = std(tempForMeanF2);
    
    stdTempF1 = [stdTempF1 stdTempF1 stdTempF1];
    stdTempF2 = [stdTempF2 stdTempF2 stdTempF2];
    
   tempForMeanF1 = mean(tempForMeanF1);
   tempForMeanF2 = mean(tempForMeanF2);
   
   tempForMeanF1 = [tempForMeanF1 tempForMeanF1 tempForMeanF1];
   tempForMeanF2 = [tempForMeanF2 tempForMeanF2 tempForMeanF2];
   
    errorbar(spacing,tempForMeanF2,stdTempF2,'r.');

    plot(spacing,tempForMeanF2,'r', 'Marker', '.','MarkerSize', 10);

    errorbar(spacing,tempForMeanF1,stdTempF1,'r.');

    plot(spacing,tempForMeanF1,'r', 'Marker', '.', 'MarkerSize', 10);

    
    if(handleA1 ~= 0 && handleA2 ~= 0 && handleA3 ~= 0)
        legend([handleA1 handleA3 handleA2],'A1', 'A3', 'A2');
        title('Comparing all conditions by phase: A1, A3, and A2');
    elseif(handleA1 ~= 0 && handleA3 ~= 0)
        legend([handleA1 handleA3],'A1', 'A3');
        title('Comparing conditions by phase: A1, and A3');
    elseif(handleA1 ~= 0 && handleA2 ~= 0)
        legend([handleA1 handleA2],'A1', 'A2');
        title('Comparing conditions by phase: A1, and A2');
    else
        close(f);
    end
    hold off;
    
end

