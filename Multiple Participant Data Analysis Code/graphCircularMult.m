function graphCircularMult(phase1, phase2, type)

%Create figure
figure;

%Plot the average of the first phase
start = plot(phase1.meanCircularF1,phase1.meanCircularF2,'r.', 'MarkerSize', 40);

%Plot the coordinates of the first phase average
text(phase1.meanCircularF1,phase1.meanCircularF2, append('\downarrow','(', num2str(phase1.meanCircularF1,'%.2f'),',',num2str(phase1.meanCircularF1,'%.2f'),')'))

%Keep the plot open
hold on;

%plot the target frequency
target = plot(phase1.targetF1, phase1.targetF2,'g.','MarkerSize',40);


%Plot the average of the second phase
condition = plot(phase2.meanCircularF1,phase2.meanCircularF2,'b.','MarkerSize', 40);


%Plot the coordinates of the second phase average
text(phase2.meanCircularF1,phase2.meanCircularF2, append('\downarrow','(', num2str(phase2.meanCircularF1,'%.2f'),',',num2str(phase2.meanCircularF1,'%.2f'),')'));

centerPhase1 = [phase1.meanCircularF1 phase1.meanCircularF2];
centerPhase2 = [phase2.meanCircularF1 phase2.meanCircularF2];


%plot the trials for the first phase in F1/F2
plot(phase1.circularF1,phase1.circularF2, 'r.','MarkerSize', 15);

viscircles(centerPhase1,phase1.stdCircular,'Color','r');
viscircles(centerPhase2,phase2.stdCircular,'Color','b');

viscircles(centerPhase1,phase1.circularTolerance,'Color','black');
viscircles(centerPhase2,phase2.circularTolerance,'Color','black');

%connect a line to the averages of both phases
line([phase1.meanCircularF1 phase2.meanCircularF1],[phase1.meanCircularF2 phase2.meanCircularF2],'Color', 'black');

%Plot the perturbation
line([phase1.meanCircularF1 phase1.targetF1],[phase1.meanCircularF2 phase1.targetF2],'Color', 'green','LineStyle', '--');


dist = norm([phase1.meanCircularF1 phase1.meanCircularF2] - [phase2.meanCircularF1 phase2.meanCircularF2]);
%Annotate the distance between the two phases
annotation('textbox','String',dist);

axis([200 1000 1200 2000]);

title(append(phase1.phase ,' and ', phase2.phase , ' F1/F2 Points for the ',type,' control'));

%Plot the second phase points in F1/F2 space
plot(phase2.circularF1,phase2.circularF2, 'b.', 'MarkerSize', 15);

%Labels and legend for plot
xlabel('F1');
ylabel('F2');
legend(append(phase1.phase ,' Phase'), append(phase2.phase,' Phase'), 'Target Frequency');
legend([start,condition,target],append(phase1.phase ,' Phase'), append(phase2.phase,' Phase'), 'Target Frequency');
ax = gca;
ax.XTick = unique( round(ax.XTick) );

hold off;

end