function graphMultiple(data, startData, phase,type)

figure;

title(append('F1 and F2 frequencies for phase: ', phase, ' for the ', type, ' control'));

xlabel('Participant Number');
ylabel('Formant Frequencies');

lengths = 1:length(data.F1);
hold on;


plot(lengths , data.F1, 'b.', 'MarkerSize',20);

plot(lengths , data.F2, 'b.', 'MarkerSize',20);

yline(data.meanF1, 'b');

yline(startData.meanF1, 'r');

yline((startData.meanF1)-3*startData.stdF1, '--black');
yline((startData.meanF1)+3*startData.stdF1, '--black');

yline((startData.meanF2)-3*startData.stdF2, '--black');
yline((startData.meanF2)+3*startData.stdF2, '--black');

yline(data.meanF2, 'b')

yline(startData.meanF2, 'r')

legend('F1 and F2 per participant', append(phase, ' Phase Mean F1 and F2'), 'Baseline Mean F1 and F2');

ax = gca;
ax.XTick = unique( round(ax.XTick) );

hold off;
end


