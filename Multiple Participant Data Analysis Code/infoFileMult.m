function infoFileMult(title,dataMult,origDataMult,config)


fileName = append(config.saveToDirectoryMult,title,'.txt');

delete(fileName);

writeAvgFormantsMult(title, dataMult.startPhase,origDataMult.startPhase)
writeAvgFormantsMult(title, dataMult.rampPhase,origDataMult.rampPhase)
writeAvgFormantsMult(title, dataMult.stayPhase,origDataMult.stayPhase)
writeAvgFormantsMult(title, dataMult.endPhase,origDataMult.endPhase)

end

