function [params, origDataMult, dataMult, finalData] = loadParticipants(type, title, condition,config)


[params, origDataMult, dataMult, finalData] = analyzeDataMult(type,condition,config);

if(config.phaseByPhaseGraph)
graphMultiple(finalData.rampPhase,finalData.startPhase,'ramp',type);

graphMultiple(finalData.stayPhase,finalData.startPhase,'stay',type);

graphMultiple(finalData.endPhase,finalData.startPhase,'end',type);
end

if(config.circularGraphMult)
graphCircularMult(finalData.startPhase, finalData.rampPhase,type);

graphCircularMult(finalData.startPhase, finalData.stayPhase,type);

graphCircularMult(finalData.startPhase, finalData.endPhase,type);
end

%infoFileMult(title,finalData,finalData);
