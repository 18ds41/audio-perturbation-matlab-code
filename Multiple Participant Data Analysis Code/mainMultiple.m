close all force;
fclose all;
clear;

config = Config_File;

spacing = [2 3 4];

if(config.responderScreen.analyze)
     [params.responderScreen, origDataMult.responderScreen, dataMult.responderScreen, finalData.responderScreen] = loadParticipants(config.responderScreen.type, config.title,  config.responderScreen, config);
elseif(config.nonResponder.analyze)
     [params.nonResponder, origDataMult.nonResponder, dataMult.nonResponder, finalData.nonResponder] = loadParticipants(config.nonResponder.type, config.title,  config.nonResponder, config);
elseif(config.follower.analyze)
     [params.follower, origDataMult.follower, dataMult.follower, finalData.follower] = loadParticipants(config.follower.type, config.title,  config.follower, config);
else

    [params.A1, origDataMult.A1, dataMult.A1, finalData.A1] = loadParticipants(append(config.A1.type), config.title,  config.A1,config);

    [params.A2, origDataMult.A2, dataMult.A2, finalData.A2] = loadParticipants(append(config.A2.type),config.title,config.A2,config);

    [params.A3, origDataMult.A3, dataMult.A3, finalData.A3] = loadParticipants(append(config.A3.type),config.title,config.A3,config);
end

if(config.conditionCompare)
    graphByPhasePerGroup(finalData,config);
end

%Save altered data
save(append(config.saveForRDirectory,config.title),'finalData');