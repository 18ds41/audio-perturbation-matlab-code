function [finalData] = mkSpecPhaseDataStructMult(finalData,type,config,params)

if(config.smoothSubjects)
    finalData.F1 = smooth(finalData.F1,config.smoothSubjectsType);
    finalData.F2 = smooth(finalData.F2,config.smoothSubjectsType);
end

if(config.subjectRMOutliers)
    [finalData.F1, finalData.subjectRemF1] = rmoutliers(finalData.F1 , 'mean');
    [finalData.F2, finalData.subjectRemF2] = rmoutliers(finalData.F2 , 'mean');
end


finalData.stdF1 = std(finalData.F1);
finalData.stdF2 = std(finalData.F2);

if(config.subjectRMOutliers)
    finalData.percLinearOutliersF1 = nnz(finalData.subjectRemF1)/size(finalData.subjectRemF1,1)*100;  
    finalData.percLinearOutliersF2 = nnz(finalData.subjectRemF2)/size(finalData.subjectRemF2,1)*100;
else
    finalData.percLinearOutliersF1 = 0;
    finalData.percLinearOutliersF2 = 0;
end

finalData.meanF1 = mean(finalData.F1);
finalData.meanF2 = mean(finalData.F2);

finalData.targetF1 = finalData.meanF1*(1 - params.F1Perturbation)*config.percCompF1/100;
finalData.targetF2 = finalData.meanF2*(1 - params.F2Perturbation)*config.percCompF2/100;


for i = 1:min(length(finalData.F1),length(finalData.F2))
        finalData.distCircular(i) = norm([finalData.F1(i)-finalData.meanF1 finalData.F2(i)-finalData.meanF2]);
end

     finalData.stdCircular = mean(finalData.distCircular);
     finalData.circularTolerance = finalData.stdCircular*config.circularToleranceMult;

if(config.circularOutliers)
for i = 1:length(finalData.distCircular)
        if(finalData.distCircular(i) > finalData.circularTolerance)
            finalData.logic(i) = 0;
        else
            finalData.logic(i) = 1;
        end
end

    finalData.logic =  logical(finalData.logic);
    
    finalData.distCircular = finalData.distCircular(finalData.logic);
    finalData.stdAlteredCircular = mean(finalData.logic);
    finalData.circularF1 =  finalData.F1(finalData.logic);
    finalData.circularF2 = finalData.F2(finalData.logic);
else
    finalData.circularF1 = finalData.F1;
    finalData.circularF2 = finalData.F2;
    if(strcmp(type,'rTMS1') == 1)
        numTrials = length(config.A1.participants);
    elseif(strcmp(type,'rTMS2') == 1)
        numTrials = length(config.A2.participants);
    elseif(strcmp(type,'rTMS3') == 1)
        numTrials = length(config.A3.participants);
    elseif(strcmp(type,'responderScreen') == 1)
        numTrials = length(config.responderScreen.participants);
    elseif(strcmp(type,'nonResponder') == 1)
        numTrials = length(config.nonResponder.participants);
    elseif(strcmp(type,'follower') == 1)
        numTrials = length(config.follower.participants);
    else
        error('Some file naming is incorrect')
        
    end
    finalData.logic = ones(1,numTrials);
end
finalData.meanCircularF1 = mean(finalData.circularF1);
finalData.meanCircularF2 = mean(finalData.circularF2);

finalData.percCircularOutliers = ((length(finalData.logic)-nnz(finalData.logic))/length(finalData.logic))*100;

end

