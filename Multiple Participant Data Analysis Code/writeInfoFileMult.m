function writeAvgFormantsMult(title,data,origData)
subjectFile = fopen( append('trialMultInfo/',title,'.txt'), 'a' );
A1 = [data.phase];

sentence = append('Info File for the %s phase \n\n');
fprintf(subjectFile,sentence,A1);

sentence = append('%s phase original mean F1 is ', num2str(origData.meanF1,'%.2f'), ' and original mean F2 is ', num2str(origData.meanF2,'%.2f'), '\n');
fprintf(subjectFile, sentence,A1);

sentence = append('%s phase altered mean F1 is ', num2str(data.meanF1,'%.2f'), ' and altered mean F2 is ', num2str(data.meanF2,'%.2f'), '\n');
fprintf(subjectFile, sentence,A1);

sentence = append('%s phase original Standard Deviation for F1 is ', num2str(origData.stdF1,'%.2f'), ' and for F2 is ', num2str(origData.stdF2,'%.2f'), '\n');
fprintf(subjectFile, sentence,A1);

sentence = append('%s phase altered Standard Deviation for F1 is ', num2str(data.stdF1,'%.2f'), ' and for F2 is ', num2str(data.stdF2,'%.2f'), '\n');
fprintf(subjectFile, sentence,A1);


sentence = append('%s phase percent of linear outliers for F1 is ', num2str(data.percLinearOutliersF1,'%.2f'), '%% and for F2 is ', num2str(data.percLinearOutliersF2,'%.2f'), '%%\n');
fprintf(subjectFile, sentence,A1);

sentence = append('%s phase number of circular outliers is ', num2str((length(data.logic)-nnz(data.logic)),'%.0f'),' out of ',num2str(length(data.logic),'%.0f') ,' (percentage is %%' ,num2str(((length(data.logic)-nnz(data.logic))/length(data.logic))*100,'%.0f') ,')\n\n');
fprintf(subjectFile, sentence,A1);

fclose(subjectFile);
fclose all;
end

