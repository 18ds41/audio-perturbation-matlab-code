# Audio Perturbation MATLAB Code


# Data Analysis in Audio Perturbation Experiments

## Overview

This program supports data analysis for Formant Perturbation experiments using the MATLAB software Audapter. It's essential for users to be familiar with Audapter before proceeding with this manual. The program is structured to assist throughout different phases of audio perturbation experiments: Start Phase, Ramp Phase, Stay Phase, and End Phase.

For a more comprehensive guide, please refer to the "Formant_Data_Analysis_Manual" PDF manual.

## Features

- Collect and manipulate data output from participants.
- Apply various mathematical manipulations and filtering techniques.
- Generate graphs for trial and phase comparisons.
- Store manipulated and original data for in-depth analysis.
- Create detailed info files post-analysis for single or multiple participants.

## Who Should Use This Software

Researchers conducting Formant Perturbation experiments with non-randomized perturbations using Audapter. It's not intended for experiments with a practice portion of the experiment.

## Prerequisites

- MATLAB
- Image Processing Toolbox MATLAB ([More Info](https://www.mathworks.com/products/image.html))
- Audapter software package (https://github.com/shanqing-cai/audapter_matlab)

## Getting Started

Edit the configuration file `Config_File` in the main code folder to match your experiment setup. The file includes automatic and manual settings sections that guide the data analysis process.

## Configuration Settings

- `config.dataDirectory`: Set the data storage location post Audapter trial.
- `config.subjectID`: Controls which trial file to open for analysis.
- `config.saveToDirectory`: Personalize data storage location post-analysis.
- `config.createInfoFile`: Toggle creation of an info file.
- `config.showParticipantInfo`: Decide if participant information should be included for blinding purposes.
- Other settings include outlier removal specifics, smoothing preferences, and trial specifications.

## MAT Files for Analysis

Data for analysis is stored in `.mat` files, which can be used for further statistical analysis. The manual includes detailed fields description for data interpretation.

## Graphs Interpretation

Graphs created during the analysis offer visual comparisons across different phases of the experiment, as well as participant compensation.

## Troubleshooting

Common errors and solutions are provided to assist with issues related to file handling and analysis process integrity.

---

For further assistance or to report issues, please contact Deven Shidfar at the email provided at the beginning of the manual.

