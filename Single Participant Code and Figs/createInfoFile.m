function createInfoFile(subjectID, data, origData,params)

global config;

fileName = append(config.infoFileLoc,subjectID,'.txt');

delete(fileName);

writeToInfoFile(subjectID, data.startPhase,origData.startPhase,params)
writeToInfoFile(subjectID, data.rampPhase,origData.rampPhase,params)
writeToInfoFile(subjectID, data.stayPhase,origData.stayPhase,params)
writeToInfoFile(subjectID, data.endPhase,origData.endPhase,params)



end

