
function graphCircular(phase1,phase2, origPhase1, origPhase2,phaseName,config)

%Create figure
figure;

%Plot the average of the first phase
baselineMean = plot(phase1.circularGraphMeanF1,phase1.circularGraphMeanF2,'r.', 'MarkerSize', 40);

%Plot the coordinates of the first phase average
%text(phase1.meanCircularF1,phase1.meanCircularF2, append('\downarrow','(', num2str(phase1.meanCircularF1,'%.2f'),',',num2str(phase1.meanCircularF1,'%.2f'),')'))

%Keep the plot open
hold on;

%Plot the average of the second phase
secondPhase = plot(phase2.circularGraphMeanF1,phase2.circularGraphMeanF2,'b.','MarkerSize', 40);

%plot target cloud
target = plot(phase1.targetF1, phase1.targetF2,'g.','MarkerSize',40);

%Plot perfect compensation
perfectComp = plot(phase1.circularPerfectTargetF1, phase1.circularPerfectTargetF2, 'c.', 'MarkerSize', 40);

%plot perturbation applied
perturb = viscircles([phase1.circularPerturbationF1, phase1.circularPerturbationF2], 40,'Color','Black','LineStyle','--');

%Plot the coordinates of the second phase average
%text(phase2.meanCircularF1,phase2.meanCircularF2, append('\downarrow','(', num2str(phase2.meanCircularF1,'%.2f'),',',num2str(phase2.meanCircularF1,'%.2f'),')'));

centerPhase1 = [phase1.circularGraphMeanF1 phase1.circularGraphMeanF2];
centerPhase2 = [phase2.circularGraphMeanF1 phase2.circularGraphMeanF2];


%plot the trials for the first phase in F1/F2
plot(phase1.circularGraphF1,phase1.circularGraphF2, 'r.','MarkerSize', 15);

viscircles(centerPhase1,phase1.stdCircular,'Color','r');
viscircles(centerPhase2,phase2.stdCircular,'Color','b');

viscircles(centerPhase1,phase1.circularTolerance,'Color','black');
viscircles(centerPhase2,phase2.circularTolerance,'Color','black');

%connect a line to the averages of both phases
line([phase1.circularGraphMeanF1 phase2.circularGraphMeanF1],[phase1.circularGraphMeanF2 phase2.circularGraphMeanF2],'Color', 'black');

%Plot the perturbation
line([phase1.circularGraphMeanF1 phase1.targetF1],[phase1.circularGraphMeanF2 phase1.targetF2],'Color', 'green','LineStyle', '--');


dist = norm([phase1.circularGraphMeanF1 phase1.circularGraphMeanF2] - [phase2.circularGraphMeanF1 phase2.circularGraphMeanF2]);
%Annotate the distance between the two phases
annotation('textbox','String',dist);


axis([200 1300 1200 2300]);

title(append(phase1.phase ,' and ', phase2.phase , ' F1/F2 Points'));

%Plot the second phase points in F1/F2 space
fg = plot(phase2.circularGraphF1,phase2.circularGraphF2, 'b.', 'MarkerSize', 15);

%Labels and legend for plot
xlabel('F1');
ylabel('F2');
legend([baselineMean,secondPhase,target,perfectComp,perturb],append(phase1.phase ,' Phase'), append(phase2.phase,' Phase'), 'Target Frequecy',"Perfect Compensation","Perturbation Applied","Location", "best");

saveas(fg,append("figsFor_",config.singleParticipantType,"\",config.subjectID,"\",phaseName,"Circular",".jpg"));

hold off;

end