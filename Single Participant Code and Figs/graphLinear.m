function graphLinear(phase1,phase2,phaseName,config)
        
        
        figure;
        
        xF1 = 1:length(phase1.F1);
        xF2 = 1:length(phase1.F2);
        
        percOfPerturbationF1 = abs(phase1.meanF1-phase2.meanF1)/abs(phase2.perfectTargetF1 - phase2.meanF1)*100;
        percOfPerturbationF2 = abs(phase1.meanF2-phase2.meanF2)/abs(phase2.perfectTargetF2 - phase2.meanF2)*100;

        if(percOfPerturbationF1 >= config.percCompF1)
            statusF1 = '(Responder or Follower, depends on which side)';
        else
            statusF1 = '(Non-responder)';
        end

        if(percOfPerturbationF2 >= config.percCompF2)
            statusF2 = '(Responder or Follower, depends on which side)';
        else
            statusF2 = '(Non-responder)';
        end

        hold on;
        
        conditionPoints = scatter(xF1, phase1.F1(:),20,'filled', 'b');
        
        title(append('First and second formant frequencies for ', phase1.phase, ' and ', phase2.phase))
        
        
        yline(phase2.meanF1, '-r');
        
        yline(phase1.meanF1,'-b', append(num2str((percOfPerturbationF1),'%.2f'),'% ', statusF1,append(' compensation is ',num2str(abs(phase2.meanF1-phase1.meanF1),'%.2f'),' Hz')));
        
        
        startLine = yline(phase2.meanF2, '-r');

        conditionLine = yline(phase1.meanF2,'-b', append(num2str((percOfPerturbationF2),'%.2f'),'% ', statusF2,append(' compensation is ',num2str(abs(phase2.meanF2-phase1.meanF2),'%.2f'),' Hz')));
        
        scatter(xF2, phase1.F2(:),20, 'filled','b');
        
        xlabel(append('Trial # in phase: ',phase1.phase));
        ylabel('Formant Frequencies (Hz)');


        targetLine = yline(phase2.targetF1, '--g');
        yline(phase2.targetF2, '--g');

        perfectTargetLine = yline(phase2.perfectTargetF1, '--k');
        yline(phase2.perfectTargetF2, '--k');

        compensationLine = yline(phase2.perturbationF1, '--c');
        yline(phase2.perturbationF2, '--c');


        legend([conditionPoints,startLine,conditionLine,targetLine,perfectTargetLine,compensationLine],append('F1 and F2 ', phase1.phase, ' per trial'),append('Mean F1 and F2 for ',phase2.phase), append('Mean F1 and F2 for ',phase1.phase), 'Target Frequencies for F1 and F2','Perfect Compensation for F1 and F2','Perturbation Applied','Location', 'best');

        ax = gca;
        ax.XTick = unique(round(ax.XTick));

        % Define the folder paths based on config settings
        baseFolder = strcat('figsFor_',config.singleParticipantType);
        subjectFolder = fullfile(baseFolder, config.subjectID);

        % Check if the subject-specific folder exists, if not, create it
        if ~exist(subjectFolder, 'dir')
            mkdir(subjectFolder); % Make the directory if it does not exist
        end

        % Define the full file path for saving the figure
        filename = fullfile(subjectFolder, strcat(phaseName, 'Linear', '.jpg'));

        % Save the figure
        saveas(ax, filename);
        hold off;

       
end

