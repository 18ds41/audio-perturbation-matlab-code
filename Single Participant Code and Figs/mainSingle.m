fclose all;
close all force;
clear;

config = Config_File();

name = strcat(config.singleParticipantType,"/",config.subjectID);

%load the raw data and analyze it
[params] = loadRawData(config,name);
[origData , data] = analyzeData(params,config);


%set the fields for num trials given from the raw data
data.startPhase.numTrials = params.startPhase.numTrials;
data.rampPhase.numTrials = params.rampPhase.numTrials;
data.stayPhase.numTrials = params.stayPhase.numTrials;
data.endPhase.numTrials = params.endPhase.numTrials;


%graph individual points
graphLinear(data.stayPhase, data.startPhase,"stay",config);
graphLinear(data.rampPhase, data.startPhase, "ramp",config);
graphLinear(data.endPhase, data.startPhase, "end",config);

%Graph F1 and F2 as vectors

graphCircular(data.startPhase, data.rampPhase, origData.startPhase, origData.rampPhase,'ramp',config);
graphCircular(data.startPhase, data.stayPhase, origData.startPhase, origData.stayPhase,'stay',config);
graphCircular(data.startPhase, data.endPhase, origData.startPhase, origData.endPhase,'end',config);

