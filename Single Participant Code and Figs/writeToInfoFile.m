function writeToInfoFile(subjectID, data, origData,params,config)

subjectFile = fopen( append('participantInfo/',subjectID,'.txt'), 'a' );
A1 = [data.phase];

fprintf(subjectFile,append('Info File for ' , subjectID,' in the ', A1, ' phase \n\n'));

if(strcmp(data.phase,'Start'))

fprintf(subjectFile,append('Max F1 perturbation is ', num2str(params.F1Perturbation, '%.3f') ,', and Max F2 perturbation is ',num2str(params.F2Perturbation, '%.3f'),'\n'));

if(config.showParticipantInfo)
    fprintf(subjectFile,append('Participants age is ', num2str(params.age,'%.0f'),'\n'));

    fprintf(subjectFile,append('Participants gender is ', params.sex, '\n'));

    fprintf(subjectFile,append('Trial time stamp is ', params.date,'\n\n'));
end

end

sentence = append('%s phase number of trials is ', num2str(data.numTrials,'%.0f'),'\n');
fprintf(subjectFile, sentence,A1);

sentence = append('%s phase original mean F1 is ', num2str(origData.meanF1,'%.2f'), ' and original mean F2 is ', num2str(origData.meanF2,'%.2f'), '\n');
fprintf(subjectFile, sentence,A1);

sentence = append('%s phase altered mean F1 is ', num2str(data.meanF1,'%.2f'), ' and altered mean F2 is ', num2str(data.meanF2,'%.2f'), '\n');
fprintf(subjectFile, sentence,A1);

sentence = append('%s phase original Linear Standard Deviation for F1 is ', num2str(origData.stdF1,'%.2f'), ' and for F2 is ', num2str(origData.stdF2,'%.2f'), '\n');
fprintf(subjectFile, sentence,A1);

sentence = append('%s phase altered Linear Standard Deviation for F1 is ', num2str(data.stdF1,'%.2f'), ' and for F2 is ', num2str(data.stdF2,'%.2f'), '\n');
fprintf(subjectFile, sentence,A1);

sentence = append('%s phase original Circular Standard Deviation is ', num2str(origData.stdCircular,'%.2f'), '\n');
fprintf(subjectFile, sentence,A1);

if(config.circularOutliers)
    sentence = append('%s phase altered Circular Standard Deviation is ', num2str(data.stdCircular,'%.2f\n'));
    fprintf(subjectFile, sentence,A1);
end

if(strcmp(data.phase,'Start') == 0)
    sentence = append('\nSubject was an F1 ', data.statusF1, ' for the %s phase\n');
    fprintf(subjectFile, sentence,A1);

    sentence = append('Subject was an F2 ', data.statusF2, ' for the %s phase');
    fprintf(subjectFile, sentence,A1);
end

fprintf(subjectFile, '\n\n//Number of Zero Formants corresponds to a complete mistrial where no formant trace is identified.\n//These trials are automatically rejected with this program.');

sentence = append('\n%s phase number of of zero formants is ', num2str(data.numZeroFormants,'%.0f'), '\n\n');
fprintf(subjectFile, sentence,A1);


sentence = append('%s phase percent of linear outliers for F1 is ', num2str(data.percLinearOutliersF1,'%.2f'), '%% and for F2 is ', num2str(data.percLinearOutliersF2,'%.2f'), '%%\n');
fprintf(subjectFile, sentence,A1);

sentence = append('%s phase number of circular outliers is ', num2str((length(data.logic)-nnz(data.logic)),'%.0f'),' out of ',num2str(length(data.logic),'%.0f') ,' (percentage is %%' ,num2str(data.percCircularOutliers,'%.0f') ,')\n\n');
fprintf(subjectFile, sentence,A1);

fclose(subjectFile);
fclose all;
end

