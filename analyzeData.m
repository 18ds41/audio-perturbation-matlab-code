function [origData,data] = analyzeData(params,config)
%ANALYZEDATA Compiles and analyzes experimental phase data for a given participant.
%   Utilizes 'mkSpecPhaseDataStruct' for structuring data per phase and 
%   'checkIfResponder' to determine response status from F1 and F2 formant data.
%   Input parameters:
%   params - a struct containing parameters for each experimental phase.
%   config - a struct containing configuration settings for the analysis.
%   Output:
%   origData - a struct containing the original organized data by phases.
%   data - a struct containing processed data and analysis results by phases.

% Initialize empty structures for holding the organized original and analyzed data.
origData = struct;
data = struct;

% Process and analyze data for each experimental phase (start, ramp, stay, end).
% The 'mkSpecPhaseDataStruct' function is expected to organize phase-specific data
% and 'checkIfResponder' determines if the participant's response meets certain criteria.
[origData.startPhase, data.startPhase] = mkSpecPhaseDataStruct(params.startPhase,params,config);
[origData.rampPhase, data.rampPhase] = mkSpecPhaseDataStruct(params.rampPhase,params,config);
[origData.stayPhase, data.stayPhase] = mkSpecPhaseDataStruct(params.stayPhase,params,config);
[origData.endPhase, data.endPhase] = mkSpecPhaseDataStruct(params.endPhase,params,config);

% Determine the response status by comparing ramp, stay, and end phase data to the start phase.
% 'checkIfResponder' assesses whether the participant's formant shifts meet expected perturbation responses.
[data.rampPhase.statusF1, data.rampPhase.statusF2] = checkIfResponder(data.rampPhase,data.startPhase,config);
[data.stayPhase.statusF1, data.stayPhase.statusF2] = checkIfResponder(data.stayPhase,data.startPhase,config);
[data.endPhase.statusF1, data.endPhase.statusF2] = checkIfResponder(data.endPhase,data.startPhase,config);

end
