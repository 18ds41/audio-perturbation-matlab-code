function [statusF1, statusF2] = checkIfResponder(comparePhase, baseline, config)
    % Function to check if the subject is a responder based on F1 and F2 values.
    % 
    % Arguments:
    % - comparePhase: A structure with mean F1 and F2 values for a given phase.
    % - baseline: A structure with baseline mean F1 and F2 values.
    % - config: A structure with responder threshold configurations.

    % Compute the differences between the phase and baseline values for F1 and F2.
    f1Diff = comparePhase.meanF1 - baseline.meanF1;
    f2Diff = comparePhase.meanF2 - baseline.meanF2;

    % Determine the responder status based on the differences and configured thresholds.
    statusF1 = getResponseStatus(f1Diff, config.thresholdF1); % Check F1 responder status
    statusF2 = getResponseStatus(f2Diff, config.thresholdF2); % Check F2 responder status
end

function status = getResponseStatus(diff, threshold)
    % Helper function to determine responder status based on a single value difference
    % and a threshold.
    %
    % - diff: The computed difference between phase and baseline mean values.
    % - threshold: The threshold to determine responder status.
    
    if abs(diff) < threshold
        status = 'Non-Responder'; % Status if difference is below the threshold
    elseif diff >= threshold
        status = 'Follower'; % Status if difference is equal or above the threshold
    else
        status = 'Responder'; % Status if difference is negative but above the threshold
    end
end
