function params = loadRawData(config,name)


params = setParams(config,name,config.single);
config.params = params;


params.startPhase.phase = "Start";
[params.startPhase] = loadRawSpecPhaseData(params.startPhase,params);

params.rampPhase.phase = "Ramp";
[params.rampPhase] = loadRawSpecPhaseData(params.rampPhase,params);

params.stayPhase.phase = "Stay";
[params.stayPhase] = loadRawSpecPhaseData(params.stayPhase,params);

params.endPhase.phase = "End";
[params.endPhase] = loadRawSpecPhaseData(params.endPhase,params);


end

