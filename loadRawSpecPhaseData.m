function [specificPhase] = loadRawSpecPhaseData(specificPhase, params)
% loadPhase Loads trial data for a specified phase
%
% Inputs:
%   specificPhase - a struct with the phase-specific information
%   phase - the name of the phase being loaded
%   params - additional parameters including 'trialsPerBlock' and file location
%
% Output:
%   specificPhase - updated struct with loaded trial data



% Load the trial data for the specific phase
for i = 1:params.trialsPerBlock:specificPhase.numTrials
    % Load trial data for the 'sustained' trials at different indexes
    % Adjust the file path as per the required format (using '\')ple
    fileDirectory = fullfile(specificPhase.fileLocation, sprintf('rep%01d', ceil(i/params.trialsPerBlock)), 'trial-1-sust.mat');
    specificPhase.trial(i) = load(fileDirectory);
    
    % Check and load the second trial if available
    if params.trialsPerBlock >= 2
        fileDirectory = fullfile(specificPhase.fileLocation, sprintf('rep%01d', ceil(i/params.trialsPerBlock)), 'trial-2-sust.mat');
        specificPhase.trial(i+1) = load(fileDirectory);
    end
    
    % Check and load the third trial if available
    if params.trialsPerBlock >= 3
        fileDirectory = fullfile(specificPhase.fileLocation, sprintf('rep%01d', ceil(i/params.trialsPerBlock)), 'trial-3-sust.mat');
        specificPhase.trial(i+2) = load(fileDirectory);
    end
end

end
