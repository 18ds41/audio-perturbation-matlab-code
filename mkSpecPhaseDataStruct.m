function [specPhaseOrigData,specPhaseData] = mkSpecPhaseDataStruct(specPhase,params,config)

specPhaseData = struct;
specPhaseOrigData = struct;

specPhaseData.numZeroFormants = 0;

startTrace = config.percOfFormantTrace(1);
endTrace = config.percOfFormantTrace(2);

specPhaseData.phase = specPhase.phase;
specPhaseOrigData.phase = specPhase.phase;

specPhaseData.numTrials = specPhase.numTrials;
specPhaseOrigData.numTrials = specPhase.numTrials;



for i = 1:specPhase.numTrials
    tempTrialF1(1:length(nonzeros(specPhase.trial(i).data.fmts(:,1))),i) = nonzeros(specPhase.trial(i).data.fmts(:,1));
    tempTrialF2(1:length(nonzeros(specPhase.trial(i).data.fmts(:,2))),i) = nonzeros(specPhase.trial(i).data.fmts(:,2));
    
    diffTrialF1 = specPhase.trial(i).data.fmts(:,1);
    diffTrialF2 = specPhase.trial(i).data.fmts(:,2);
    
    indexStartF1 = min(find(diffTrialF1));
    indexEndF1 = max(find(diffTrialF1));
    
    indexStartF2 = min(find(diffTrialF2));
    indexEndF2 = max(find(diffTrialF2));
    
    
    diffTrialF1 = diffTrialF1(indexStartF1+1:indexEndF1-1);
    diffTrialF2 = diffTrialF2(indexStartF2+1:indexEndF2-1);
    
    endTempF1 = size(nonzeros(tempTrialF1(:,i)));
    endTempF2 = size(nonzeros(tempTrialF2(:,i)));
    
    sizeF1 = length(ceil(endTempF1*startTrace:endTempF1*endTrace));
    sizeF2 = length(ceil(endTempF2*startTrace:endTempF2*endTrace));
    
    if(config.remDiscontinuousTrials)
        diffTempF1 = max(diff(diffTrialF1));
        diffTempF2 = max(diff(diffTrialF2));
    else
        diffTempF1 = 0;
        diffTempF2 = 0;
    end
    
    if(endTempF1 >= 1 & endTempF2 >= 1 & (diffTempF1 < 250 | diffTempF2 < 250))
        specPhaseData.trialF1(1:sizeF1,i) = tempTrialF1(ceil(endTempF1*startTrace:endTempF1*endTrace),i);
        specPhaseData.trialF2(1:sizeF2,i) = tempTrialF2(ceil(endTempF2*startTrace:endTempF2*endTrace),i);
    else
        specPhaseData.numZeroFormants = specPhaseData.numZeroFormants + 1;
        specPhaseData.trialF1(:,i) = 0;
        specPhaseData.trialF2(:,i) = 0;
    end
    
    if((diffTempF1 < 250 | diffTempF2 < 250))
        specPhaseData.A(1,i) = diffTempF1;
        specPhaseData.A(2,i) = diffTempF1;
    else
        specPhaseData.A(:,i) = 0;
    end
    
    specPhaseOrigData.rawF1(1:length(specPhase.trial(i).data.fmts(:,1)),i) = specPhase.trial(i).data.fmts(:,1);
    specPhaseOrigData.rawF2(1:length(specPhase.trial(i).data.fmts(:,2)),i) = specPhase.trial(i).data.fmts(:,2);
    
    specPhaseOrigData.F1(i) = mean(nonzeros(specPhaseOrigData.rawF1(:,i)));
    specPhaseOrigData.F2(i) = mean(nonzeros(specPhaseOrigData.rawF2(:,i)));
    
    if(config.smoothTrace)
        tempF1 = transpose(smooth((nonzeros(specPhaseData.trialF1(:,i))),config.smoothTraceType));
        tempF2 = transpose(smooth((nonzeros(specPhaseData.trialF2(:,i))),config.smoothTraceType));
    else
        tempF1 =  nonzeros(specPhaseData.trialF1(:,i));
        tempF2 =  nonzeros(specPhaseData.trialF2(:,i));
    end
    
    if(config.trialRMOutliers)
    [notherTempF1,specPhaseOrigData.remTrialF1] = rmoutliers(tempF1,config.trialRMOutliersType);
    [notherTempF2, specPhaseOrigData.remTrialF2] = rmoutliers(tempF2,config.trialRMOutliersType);
    blessedF1 = mean(notherTempF1);
    blessedF2 = mean(notherTempF2);         
    newF1(i) = blessedF1;
    newF2(i) = blessedF2;
    else
    specPhaseOrigData.remTrialF1 = zeros(length(specPhase.numTrials));
    specPhaseOrigData.remTrialF2 = zeros(length(specPhase.numTrials));
    newF1(i) = mean(tempF1);
    newF2(i) = mean(tempF2);
    end
end

    specPhaseOrigData.F1 = rmmissing(specPhaseOrigData.F1);
    specPhaseOrigData.F2 = rmmissing(specPhaseOrigData.F2);

    specPhaseData.F1 = rmmissing(newF1);
    specPhaseData.F2 = rmmissing(newF2);
    
    
   
    
    if(specPhase.specifyBool)
        specPhase.endingTrialF1 = min(specPhase.endingTrial-specPhase.beginTrial,length(specPhaseData.F1)-specPhase.beginTrial);
        specPhase.endingTrialF2 = min(specPhase.endingTrial-specPhase.beginTrial,length(specPhaseData.F2)-specPhase.beginTrial);
        specPhaseData.F1 = specPhaseData.F1(specPhase.beginTrial:specPhase.endingTrialF1+specPhase.beginTrial);
        specPhaseData.F2 = specPhaseData.F2(specPhase.beginTrial:specPhase.endingTrialF2+specPhase.beginTrial);
    end
    

    specPhaseData.circularGraphF1 = specPhaseData.F1;
    specPhaseData.circularGraphF2 = specPhaseData.F2;

    specPhaseData.circularGraphMeanF1 = mean(specPhaseData.circularGraphF1);
    specPhaseData.circularGraphMeanF2 = mean(specPhaseData.circularGraphF2);

    specPhaseData.circularF1 = specPhaseData.F1;
    specPhaseData.circularF2 = specPhaseData.F2;

    specPhaseData.meanCircularF1 = mean(specPhaseData.circularF1);
    specPhaseData.meanCircularF2 = mean(specPhaseData.circularF2);
    
if(config.phaseRMOutliers)
    [notherTempF1, specPhaseOrigData.remPhaseF1] = rmoutliers((specPhaseData.F1),config.phaseRMOutliersType);
    [notherTempF2, specPhaseOrigData.remPhaseF2] = rmoutliers((specPhaseData.F2),config.phaseRMOutliersType);
    specPhaseOrigData.tempRemF1 = ~specPhaseOrigData.remPhaseF1;
    specPhaseOrigData.tempRemF2 = ~specPhaseOrigData.remPhaseF2;
    specPhaseOrigData.tempRem = bitand(specPhaseOrigData.tempRemF1,specPhaseOrigData.tempRemF2);
    
    
    specPhaseData.F1 = specPhaseData.F1(specPhaseOrigData.tempRem);
    specPhaseData.F2 = specPhaseData.F2(specPhaseOrigData.tempRem);
    
    specPhaseData.meanF1 = mean(notherTempF1);
    specPhaseData.meanF2 = mean(notherTempF2);
else
    
    specPhaseData.meanF1 = mean(specPhaseData.F1);
    specPhaseData.meanF2 = mean(specPhaseData.F2);
    specPhaseOrigData.remPhaseF1 = zeros(length(specPhase.numTrials));
    specPhaseOrigData.remPhaseF2 = zeros(length(specPhase.numTrials));
end
    


    specPhaseOrigData.meanF1 = mean(specPhaseOrigData.F1);
    specPhaseOrigData.meanF2 = mean(specPhaseOrigData.F2);

    specPhaseData.circularPerfectTargetF1 = specPhaseData.meanF1 - specPhaseData.meanF1*(params.F1Perturbation);
    specPhaseData.circularPerfectTargetF2 = specPhaseData.meanF2 - specPhaseData.meanF2*(params.F2Perturbation);

    specPhaseData.circularPerturbationF1 = specPhaseData.meanF1 + specPhaseData.meanF1*(params.F1Perturbation);
    specPhaseData.circularPerturbationF2 = specPhaseData.meanF2 + specPhaseData.meanF2*(params.F2Perturbation);

    specPhaseData.targetF1 = specPhaseData.meanF1 - specPhaseData.meanF1*(params.F1Perturbation)*config.percCompF1/100;
    specPhaseData.targetF2 = specPhaseData.meanF2 - specPhaseData.meanF2*(params.F2Perturbation)*config.percCompF2/100;

    specPhaseData.perfectTargetF1 = specPhaseData.meanF1*(1 - params.F1Perturbation);
    specPhaseData.perfectTargetF2 = specPhaseData.meanF2*(1 - params.F2Perturbation);

    specPhaseData.perturbationF1 = specPhaseData.meanF1*(1+params.F1Perturbation);
    specPhaseData.perturbationF2 = specPhaseData.meanF2*(1+params.F2Perturbation);

    specPhaseData.stdF1 = std(specPhaseData.F1);
    specPhaseData.stdF2 = std(specPhaseData.F2);

    specPhaseOrigData.stdF1 = std(specPhaseOrigData.F1);
    specPhaseOrigData.stdF2 = std(specPhaseOrigData.F2);

    specPhaseData.percLinearOutliersF1 = nnz(specPhaseOrigData.remPhaseF1)/specPhase.numTrials*100;
    specPhaseData.percLinearOutliersF2 = nnz(specPhaseOrigData.remPhaseF2)/specPhase.numTrials*100;

    specPhaseOrigData.circularF1 = specPhaseOrigData.F1;
    specPhaseOrigData.circularF2 = specPhaseOrigData.F2;
    
    specPhaseOrigData.meanCircularF1 = mean(specPhaseOrigData.F1);
    specPhaseOrigData.meanCircularF2 = mean(specPhaseOrigData.F2);
   
    for i = 1:min(length(specPhaseData.circularF1),length(specPhaseData.circularF2))
        specPhaseData.distCircular(i) = norm([specPhaseData.circularF1(i)-specPhaseData.meanCircularF1 specPhaseData.circularF2(i)-specPhaseData.meanCircularF2]);
        specPhaseOrigData.distCircular(i) = norm([specPhaseOrigData.circularF1(i)-specPhaseOrigData.meanCircularF1 specPhaseOrigData.circularF2(i)-specPhaseOrigData.meanCircularF2]);

    end

    specPhaseOrigData.stdCircular = mean(specPhaseOrigData.distCircular);

    specPhaseData.stdCircular = mean(specPhaseData.distCircular);
    
    specPhaseData.circularTolerance = specPhaseData.stdCircular*config.circularTolerance;

if(config.circularOutliers)

    for i = 1:length(specPhaseData.distCircular)
        if(specPhaseData.distCircular(i) > specPhaseData.circularTolerance)
            specPhaseData.logic(i) = 0;
        else
            specPhaseData.logic(i) = 1;
        end
    end

    specPhaseData.logic =  logical(specPhaseData.logic);
    
    specPhaseData.distCircular = specPhaseData.distCircular(specPhaseData.logic);
    specPhaseData.stdCircular = mean(specPhaseData.distCircular);

    specPhaseData.circularF1 =  specPhaseData.circularF1(specPhaseData.logic);
    specPhaseData.circularF2 = specPhaseData.circularF2(specPhaseData.logic);


    specPhaseData.circularGraphF1 =  specPhaseData.circularGraphF1(specPhaseData.logic);
    specPhaseData.circularGraphF2 = specPhaseData.circularGraphF2(specPhaseData.logic);
    specPhaseData.circularGraphMeanF1 = mean(specPhaseData.circularGraphF1);
    specPhaseData.circularGraphMeanF2 = mean(specPhaseData.circularGraphF2);

else
    specPhaseData.logic = ones(1,specPhaseData.numTrials);
end

specPhaseData.meanCircularF1 = mean(specPhaseData.circularF1);
specPhaseData.meanCircularF2 = mean(specPhaseData.circularF2);

specPhaseData.percCircularOutliers = ((length(specPhaseData.logic)-nnz(specPhaseData.logic))/length(specPhaseData.logic))*100;

end
