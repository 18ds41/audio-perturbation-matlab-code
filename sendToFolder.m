function sendToFolder(data, config)
% sendToFolder Saves data to a folder based on the phase status
%
%   sendToFolder(data, config) saves the 'data' structure to a folder. If
%   the data for stayPhase has statusF1 or statusF2 marked as '(Follower)' 
%   or '(Non-Responder)', it saves to an 'ExcludedGroup' directory. 
%   Otherwise, it saves to an 'IncludedGroup' directory, in a subfolder 
%   based on a random selection between 1 to 3.

% Directory reference from config structure
excludePath = fullfile(config.saveForRDirectory, 'ExcludedGroup', config.subjectID);
includePathPrefix = fullfile(config.saveForRDirectory, 'IncludedGroup'); % Changed 'A' to 'IncludedGroup' for clarity

% Check the conditions for exclusion
isExcluded = (strcmp(data.stayPhase.statusF1,'(Follower)') || strcmp(data.stayPhase.statusF1, '(Non-Responder)')) && ...
             (strcmp(data.stayPhase.statusF2,'(Follower)') || strcmp(data.stayPhase.statusF2, '(Non-Responder)'));

% Perform saving operation based on the condition
if isExcluded
    % Save to the 'ExcludedGroup' if conditions are met
    save(excludePath, 'data');
else
    % Save to a random subfolder (1 to 3) in the 'IncludedGroup' if not excluded
    num = randi([1, 3], 1);
    includePath = fullfile(includePathPrefix, num2str(num,'%i'), config.subjectID);
    save(includePath, 'data');
end

end
