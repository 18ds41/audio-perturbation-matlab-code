function [params] = setParams(config, name, single)
    % setParams function configures the experiment parameters for a given subject.
    % Inputs:
    %    config - configuration settings for the experiment.
    %    name - name of the subject or experimental data file.
    %    single - boolean to determine whether to load single or multiple participant data.

    % Determine the correct directory based on the 'single' flag
    %Load all data
    if(single == true)
        findReps = load(append(config.dataDirectory,name,"\expt"));
    elseif(single == false)
        findReps = load(append(config.dataDirectoryMult,name,"\expt"));
        disp(append(config.dataDirectoryMult,name,"\expt"))
    end


    % Extract experiment configuration details into the 'params' struct
    exptConfig = findReps.expt.subject.expt_config;
    params.trialsPerBlock = exptConfig.SUST_TRIALS_PER_BLOCK;
    params.date = exptConfig.EXPT_DATE;
    params.sex = exptConfig.SUBJECT_GENDER;
    params.age = exptConfig.SUBJECT_AGE;
    
    params.F1Perturbation = findReps.expt.subject.expt_config.SUST_F1_SHIFTS_RATIO;
    params.F2Perturbation = findReps.expt.subject.expt_config.SUST_F2_SHIFTS_RATIO;
    
    params.F1Perturbation = erase(params.F1Perturbation,'{sust-[');
    params.F2Perturbation = erase(params.F2Perturbation,'{sust-[');
    
    params.F1Perturbation = str2num(erase(params.F1Perturbation,']}'));
    params.F2Perturbation = str2num(erase(params.F2Perturbation,']}'));
    
    % # of trials per Phase
    params.startPhase.numTrials = findReps.expt.subject.expt_config.SUST_START_REPS*params.trialsPerBlock;
    params.rampPhase.numTrials = findReps.expt.subject.expt_config.SUST_RAMP_REPS*params.trialsPerBlock;
    params.stayPhase.numTrials = findReps.expt.subject.expt_config.SUST_STAY_REPS*params.trialsPerBlock;
    params.endPhase.numTrials = findReps.expt.subject.expt_config.SUST_END_REPS*params.trialsPerBlock;
    
    %set the file locations for the respective phases
    if(single == true)
    params.startPhase.fileLocation = append(config.dataDirectory,config.subjectID,'\start\');
    params.rampPhase.fileLocation = append(config.dataDirectory,config.subjectID,'\ramp\');
    params.stayPhase.fileLocation = append(config.dataDirectory,config.subjectID,'\stay\');
    params.endPhase.fileLocation = append(config.dataDirectory,config.subjectID,'\end\');
    else
        
    params.startPhase.fileLocation = append(config.dataDirectoryMult,name,'\start\');
    params.rampPhase.fileLocation = append(config.dataDirectoryMult,name,'\ramp\');
    params.stayPhase.fileLocation = append(config.dataDirectoryMult,name,'\stay\');
    params.endPhase.fileLocation = append(config.dataDirectoryMult,name,'\end\');
    end
    
    if(config.specifyStart)
    params.startPhase.beginTrial = config.startBeginTrial;
    params.startPhase.endingTrial = config.startEndingTrial;
    end
    
    if(config.specifyRamp)
    params.rampPhase.beginTrial = config.rampBeginTrial;
    params.rampPhase.endingTrial = config.rampEndingTrial;
    end
    
    if(config.specifyStay)
    params.stayPhase.beginTrial = config.stayBeginTrial;
    params.stayPhase.endingTrial = config.stayEndingTrial;
    end
    
    if(config.specifyEnd)
    params.endPhase.beginTrial = config.endBeginTrial;
    params.endPhase.endingTrial = config.endEndingTrial;
    end
    
    params.startPhase.specifyBool = config.specifyStart;
    params.rampPhase.specifyBool = config.specifyRamp;
    params.stayPhase.specifyBool = config.specifyStay;
    params.endPhase.specifyBool = config.specifyEnd;
    
    if(config.endPhaseFail)
        params.endPhase.numTrials = config.endPhaseTrial;
    end
    
    end